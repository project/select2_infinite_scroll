(function($, Drupal) {
  Drupal.behaviors.infiniteScrollSelect2Init = {
    attach: function (context) {
      $('select.select2-infinite-scroll', context).once('select2-init').each(function () {
        let that = $(this);
        let fieldName = that.attr('name');
        let s2isSettings = drupalSettings.s2is[fieldName];
        that.select2({
          allowClear: s2isSettings.allowClear || false,
          placeholder: s2isSettings.placeholder || Drupal.t('- Select -'),
          ajax: {
            // Our middleware endpoint.
            url: s2isSettings.endpoint || window.location.origin + drupalSettings.path.baseUrl + 'api/select2-infinite-scroll-widget-options',
            type: 'get',
            dataType: 'json',
            // delay: 100, // Throttle the response.
            data: function (params) {
              // This defines the post parameters we'll pass along to our endpoint.
              return {
                s2is: {
                  keyword: params.term,
                  page: params.page || 1,
                  pageSize: s2isSettings.pageSize || 100,
                  cacheTime: s2isSettings.cacheTime || 300,
                  fieldName: fieldName,
                  callback: s2isSettings.callback || null,
                }
              };
            }
          }
        });
      });
    }
  };
})(jQuery, Drupal);
