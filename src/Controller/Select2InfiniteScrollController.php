<?php

namespace Drupal\select2_infinite_scroll\Controller;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\select2_infinite_scroll\Element\Select2InfiniteScroll;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use function Drupal\Tests\Core\Render\callback;

/**
 * Default controller for select2 infinite scroll result sets.
 */
class Select2InfiniteScrollController extends ControllerBase {

  /**
   * The default cache bin.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected CacheBackendInterface $cache;

  /**
   * Constructs the controller.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend service.
   */
  public function __construct(CacheBackendInterface $cache) {
    $this->cache = $cache;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('cache.default')
    );
  }

  /**
   * Attempts to retrieve widget options for the given parameters.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   *
   * @throws \Exception
   */
  public function getWidgetOptions(Request $request) {
    // Filter out all non-options, and make sure they're not empty strings.
    $query_params = $request->query->all();
    $options = array_filter($query_params['s2is'] ?? [], function ($value, $key) {
      return !empty($value) && $value != 0 && in_array($key, [
          'callback',
          'fieldName',
          'cacheTime',
          'page',
          'pageSize',
          'keyword',
          'selected',
        ]);
    }, ARRAY_FILTER_USE_BOTH);

    if (!isset($options['callback'])) {
      return new JsonResponse([
        'message' => (string) $this->t('The parameter "callback" was missing from the s2is post values.'),
      ], 400);
    }

    $options['page'] = $options['page'] ?? Select2InfiniteScroll::FIRST_PAGE;
    $options['pageSize'] = $options['pageSize'] ?? Select2InfiniteScroll::PAGE_SIZE;

    // Create a cache key with hashed options.
    $cache_key = 's2is.field_name:'
      . md5($options['fieldName'] ?? random_bytes(10))
      . '.options:' . md5(serialize($options));
    if (!$cached_options = $this->cache->get($cache_key)) {
      $results = call_user_func(explode('::', $options['callback']), $options);

      $select2_options = [];
      foreach ($results as $id => $value) {
        $select2_options[] = [
          'id' => $id,
          'text' => $value,
        ];
      }

      // Cache for up to 5 minutes so repeated usage does not result in a crazy
      // amount of calls to the API endpoint.
      $this->cache->set($cache_key, $select2_options, time() + ($options['cacheTime'] ?? Select2InfiniteScroll::CACHE_TIME));
      $cached_options = new \stdClass();
      $cached_options->data = $select2_options;
    }

    $select2_options = $cached_options->data;
    $has_more_results = count($select2_options) >= $options['pageSize'];
    return new JsonResponse([
      'results' => $select2_options,
      'pagination' => [
        'more' => $has_more_results,
      ],
    ]);
  }

}
