<?php

namespace Drupal\select2_infinite_scroll\Element;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Select;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;

/**
 * Provides an infinite scroll select2 element.
 *
 * Properties (on top of the default select element's properties):
 * - #callback: Callback to retrieve the result set from, has to be a full class name + method (statically called).
 * - #endpoint: Optionally, instead of providing a resultset callback, you can provide a custom endpoint route name.
 * - #cache_time: The time the result set will be cached for (default: 300 seconds).
 * - #allow_clear: Allows the select box to be cleared.
 * - #placeholder: Replaces the "#empty_option" option.
 * - #page_size: the amount of results per page.
 *
 * Usage example:
 *
 * @code
 * $form['title'] = [
 *   '#type' => 'select2_infinite',
 *   '#title' => $this->t('Select tags'),
 *   '#default_value' => 3,
 *   '#callback' => ['::resultSet'],
 *   '#cache_time' => 900,
 *   '#select2_options' => [
 *     '1' => $this->t('One'),
 *     '2' => [
 *       '2.1' => $this->t('Two point one'),
 *       '2.2' => $this->t('Two point two'),
 *     ],
 *     '3' => $this->t('Three'),
 *   ],
 *   '#allow_clear' => TRUE,
 *   '#placeholder' => $this->t('- Select a value -'),
 *   '#page_size' => 25,
 *   '#required' => TRUE,
 * ];
 * @endcode
 *
 * @FormElement("select2_infinite_scroll")
 */
class Select2InfiniteScroll extends Select {

  const FIRST_PAGE = 1;
  const PAGE_SIZE = 100;
  const CACHE_TIME = 300;

  public function getInfo() {
    $info = [
      '#theme' => 'select2_infinite',
      '#select2_options' => [],
    ] + parent::getInfo();

    unset($info['#options']);
    return $info;
  }

  /**
   * {@inheritDoc}
   */
  public static function processSelect(&$element, FormStateInterface $form_state, &$complete_form) {
    $element = parent::processSelect($element, $form_state, $complete_form);
    if (isset($element['#empty_option'])) {
      unset($element['#empty_option']);
      unset($element['#empty_value']);
    }

    if (!isset($element['#name'])) {
      throw new \BadMethodCallException(t('The element is not properly configured, no field name could be extracted.'));
    }

    $element['#attributes']['class'][] = 'select2-infinite-scroll';
    $element['#attributes']['data-s2is-field-name'] = $element['#name'];
    // We need to perform custom validation!
    $element['#element_validate'] = [
      [static::class, 'validateElement'],
    ];

    // Send the appropriate options to the javascript for further processing.
    foreach (['callback', 'endpoint', 'cache_time', 'allow_clear', 'placeholder', 'page_size'] as $option) {
      if (!isset($element['#' . $option])) {
        continue;
      }

      if ($option === 'placeholder' && $element['#' . $option] instanceof TranslatableMarkup) {
        $element['#' . $option] = (string) $element['#' . $option];
      }
      elseif ($option === 'endpoint') {
        $url = Url::fromRoute($element['#' . $option]);
        $element['#' . $option] = $url->setAbsolute(TRUE)->toString();
      }

      // Turn the option string into a lower camelcase value.
      $javascript_key = lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $option))));
      $complete_form['#attached']['drupalSettings']['s2is'][$element['#name']][$javascript_key] = $element['#' . $option];
    }

    $complete_form['#attached']['library'][] = 'select2_infinite_scroll/select2';

    return $element;
  }

  /**
   * Validates the select2 infinite scroll element.
   *
   * @param $element
   *   The element array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param $complete_form
   *   The complete form.
   */
  public static function validateElement(&$element, FormStateInterface $form_state, &$complete_form) {
    $values = $form_state->getValues();
    $value = NestedArray::getValue($values, $element['#parents']);

    $value_exists = FALSE;
    if (isset($element['#callback']) && ($callback = $element['#callback'])) {
      $options = [
        'selected' => $value,
      ];

      $results = call_user_func(explode('::', $callback), $options);
      if (in_array($value, array_keys($results))) {
        $value_exists = TRUE;
      }
    }

    if (!$value_exists) {
      $form_state->setError($element, t('An illegal choice has been detected. Please contact the site administrator.'));
    }
  }

}
