<?php

namespace Drupal\select2_infinite_scroll_example\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\select2_infinite_scroll\Element\Select2InfiniteScroll;

/**
 * Example form to demonstrate the capabilities of select 2 infinite scroll.
 */
class ExampleForm extends FormBase {

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'select2_infinite_scroll_example_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $input = $form_state->getUserInput();
    $values = $form_state->getValues();
    $default_value = $input['example'] ?? $values['example'] ?? NULL;

    $form['example'] = [
      '#type' => 'select2_infinite_scroll',
      '#title' => $this->t('Select2 Infinite Scroll'),
      // We don't need any options in the select element by default.
      '#select2_options' => [],
      // Cache the result up to 90 seconds.
      '#cache_time' => 90,
      // Allow the user to clear their choice.
      '#allow_clear' => TRUE,
      // The callback for the result set.
      '#callback' => static::class . '::exampleResultSet',
      // The amount of results per page.
      '#page_size' => 25,
      // The placeholder when no value was selected.
      '#placeholder' => $this->t('- Choose -'),
    ];

    // Make sure the default value is present in the html select options.
    if ($default_value) {
      $form['example']['#select2_options'][$default_value] = $default_value;
    }

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->messenger()->addMessage($this->t('The submitted value is %value', [
      '%value' => $form_state->getValue('example'),
    ]));
  }

  /**
   * The result set callback.
   *
   * As an example we're simply returning a range of numbers, but in a real
   * situation this would be some API callback, being Drupal entities or an
   * external API call.
   *
   * @param $options
   *   The options to define what results to show.
   *
   * @return array
   *   The result set.
   */
  public static function exampleResultSet($options) {
    // If a search term was provided, we'll just return that.
    // But in a real situation, you would need to do some querying based on
    // the requirements of your environment, and return the results of that.
    if (isset($options['keyword'])) {
      if (is_numeric($options['keyword'])) {
        return [
          $options['keyword'] => $options['keyword'],
        ];
      }
    }

    // Get the page and page size parameters, pagination is done within this
    // result set callback.
    $page = ($options['page'] ?? Select2InfiniteScroll::FIRST_PAGE) - 1;
    $page_size = $options['pageSize'] ?? Select2InfiniteScroll::PAGE_SIZE;

    $start = $page * $page_size;
    // Add 0 or 1 based on whether page is higher than 0 or not.
    $extra = intval(boolval($page));
    $range = range($start + $extra, $start + $page_size);

    // Return the selected value as well, if requested.
    // In a real situation, you would need to perform some validation here!
    if (isset($options['selected']) && is_numeric($options['selected'])) {
      $range[$options['selected']] = $options['selected'];
    }

    return array_combine($range, $range);
  }

}
